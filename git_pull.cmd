@echo off
setlocal EnableDelayedExpansion
set "RemoteName="
set IsWaitFlag=0
for %%i in (%*) do (
  if "!IsWaitFlag!" equ "1" (
    set/a IsWaitFlag+=%%i
  )
  if "%%i" equ "--wait" (
    set/a IsWaitFlag+=1
  )
  if "!RemoteName!" equ "" (
    set RemoteName=%%i
  )
)
set "branch="
for /f "delims=" %%I in ('git rev-parse --abbrev-ref HEAD') do (
  set "branch=%%I"
)
echo Now Branch:!branch!
set RemoteCount=0
for /f %%i in ('git remote 2^>nul') do (
  set/a RemoteCount+=1
  echo * [32mremote:[0m %%i
  for /f "delims=" %%j in ('git remote show %%i ^| find "Fetch URL:"') do (
    echo %%j
  )
)
if "!RemoteCount!" equ "0" (
  echo Remote is not exist
  goto:END
) else (
  :: confirm is gitrepo
  call %userprofile%\.shell\git_dir_add.cmd
)

:: confirm repo name exist
for /f "tokens=*" %%i in ('echo !RemoteName! ^| find "-"') do (
  set RemoteName=
)

if "!RemoteName!" equ "" (
  echo Here Remote List:
  set RemoteCount=0
  for /f %%i in ('git remote') do (
    set/a RemoteCount+=1
  )
  if "!RemoteCount!" equ "0" (
    echo "No remote"
  ) else if "!RemoteCount!" equ "1" (
    call:pullFirstOne
  ) else (
    git remote
    set /p remote_item=Input a remote:
    if "!remote_item!" equ "" (
      echo Goto pull FirstOne...
      call:pullFirstOne
    ) else (
      git pull !remote_item! !branch!
    )
  )
) else (
  if "!RemoteName!" equ "first" (
    call:pullFirstOne
  ) else (
    git pull !RemoteName! !branch!
  )
)
goto:END

:pullFirstOne
for /f "tokens=*" %%i in ('git remote') do (
  echo.
  echo Now Pull %%i
  echo.
  git pull %%i !branch!
  goto:eof
)
goto:eof

:END
set WAIT_SEC=0
if "!IsWaitFlag!" equ "0" (
  goto:eof
) else if "!IsWaitFlag!" equ "1" (
  set WAIT_SEC=9
) else (
  set/a WAIT_SEC+=!IsWaitFlag!-1
)

if %WAIT_SEC% gtr 9 (
  set WAIT_SEC=9
)
call:getbs bs
set/p="Wait to Exit: "<nul
for /l %%i in (%WAIT_SEC%,-1,0) do (
  set /p="%%i"<nul
  choice /t 1 /d y /n >nul
  set /p="%bs%"<nul
)
goto:eof

:getbs
  for /F %%a in ('"prompt $h&for %%b in (1) do rem"')do Set "%~1=%%a"
goto:eof

:eof
endlocal
