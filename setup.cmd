@echo off
title BsWinSetup
:: utf-8 65001 en 437 zh 936
chcp 65001 > nul
setlocal EnableDelayedExpansion

net session 2>nul>nul
if %errorlevel% equ 0 (
  echo.
  goto:start
) else (
  echo [31mRequires administrator to run[0m
  pause
  goto:eof
)

:start
set osVersion=
ver | findstr /r /i "[* 6.1.*]" > nul && set osVersion=Win7&& goto:end_osv
ver | findstr /r /i "[* 10.0.*]" > nul && set osVersion=Win10&& goto:end_osv
:end_osv

echo "===================================="
echo "======= Init Bin-Win64 Dir ========="
echo "===================================="
if not exist %programdata%\Bin-Win64 (mkdir %programdata%\Bin-Win64&&echo Mkdir Bin-Win64 end&&pause)
if not exist C:\Programs (mkdir C:\Programs&&echo Mkdir Programs end&&pause)

if "!osVersion!" equ "Win10" ( powershell -c "Add-MpPreference -ExclusionPath %programdata%\Bin-Win64" )
if "!osVersion!" equ "Win10" ( powershell -c "Add-MpPreference -ExclusionPath C:\Programs" )
if "!osVersion!" equ "Win10" ( powershell -c "Add-MpPreference -ExclusionPath D:\BSOS" )

echo "===================================="
echo "====== Download Common.cmd ========="
echo "===================================="
cd /d %programdata%\Bin-Win64

if "!osVersion!" neq "Win10" (
  echo [31mThis OS is not Win10, download by certutil![0m
  certutil.exe -urlcache -split -f http://gitee.com/bsos-top/open/raw/master/curl.exe
  if !errorlevel! neq 0 (
    echo "Download curl.cmd error"
    goto:eof
  )
)
if not exist unix2dos.exe (
  curl -#fLO https://gitee.com/bsos-top/open/raw/master/unix2dos.exe --ssl-no-revoke
  if !errorlevel! neq 0 (
    echo "Download unix2dos.exe error"
    goto:eof
  )
)
if not exist common.cmd (
  echo "Download common.cmd"
  curl -#fLO https://gitee.com/bsos-top/open/raw/master/common.cmd --ssl-no-revoke
  if !errorlevel! neq 0 (
    echo "Download common.cmd error"
    goto:eof
  )
  unix2dos.exe -f common.cmd
) else (
  echo "[*****]File:common.cmd is exist"
)

echo "===================================="
echo "======= Main App  in Windows ======="
echo "===================================="
call:download wgetPack.exe
if not exist wget.exe (
  wgetPack.exe
)
call:download jqPack.exe
if not exist jq.exe (
  jqPack.exe
)
call:download 7zPack.exe
if not exist 7z.exe (
  7zPack.exe
)
if not exist gocr.exe (
  wget --no-check-certificate https://www.winstonkenny.cn/d/OlRsxL89 -O gocr.exe
)
if not exist wsr.exe (
  wget --no-check-certificate https://www.winstonkenny.cn/d/HfyWqJdB -O wsr.exe
)
if not exist vim82 (
  call:download vim82.zip
  7z.exe x vim82.zip
  del vim82.zip
  del vim82.zip.md5
  call:download vim.cmd
)

if not exist clink (
  call:download clink.exe
  7z.exe x clink.exe
  del clink.exe
)

echo "===================================="
echo "===== Linux Command in Windows ====="
echo "===================================="
if not exist linuxPack.exe (
  call:download linuxPack.exe
  linuxPack.exe
)

echo "===================================="
echo "====== Manual App in Windows ======="
echo "===================================="
call:download scst.exe

echo "===================================="
echo "==== Manual Command in Windows ====="
echo "===================================="
call:download ch.cmd
call:download cdsys.cmd
call:download cdsu.cmd
call:download cdpd.cmd
call:download cdpb.cmd
call:download cdps.cmd
call:download cdc.cmd
call:download cdd.cmd
call:download cde.cmd
call:download cdf.cmd
call:download open.cmd
call:download ll.cmd
call:download senv.cmd
call:download run.cmd
call:download run.vbs

echo "===================================="
echo "===== Manual Config in Windows ====="
echo "===================================="
if exist %SystemRoot%\System32\senv.cmd (
  set SetSenv=n
  set /p SetSenv="File:senv.cmd is already in SystemRoot32, replace?[y/N]"
  if !SetSenv! equ y (
    del %SystemRoot%\System32\senv.cmd
  )
)

if not exist %SystemRoot%\System32\senv.cmd (
  mklink %SystemRoot%\System32\senv.cmd %programdata%\Bin-Win64\senv.cmd
)

if exist "%programdata%\Microsoft\Windows\Start Menu\Programs\Startup"\run.vbs (
  set SetReplace=n
  set /p SetReplace="File:run.vbs is already in Startup, replace?[y/N]"
  if !SetReplace! equ y (
    attrib -r -s "%programdata%\Microsoft\Windows\Start Menu\Programs\Startup\run.vbs"
  )
)

copy /Y run.vbs "%programdata%\Microsoft\Windows\Start Menu\Programs\Startup" 2>nul>nul
attrib +r +s "%programdata%\Microsoft\Windows\Start Menu\Programs\Startup\run.vbs"

set SetFirewall=n
set /p SetFirewall=SetFirewall[y/N]
if !SetFirewall! equ y (
  echo Set tcp:22:in
  netsh.exe advfirewall firewall add rule name="sshd" dir=in protocol=tcp localport=22 action=allow
  echo Set tcp:8080:in
  netsh.exe advfirewall firewall add rule name="serv" dir=in protocol=tcp localport=8080 action=allow
  echo Set tcp:8080:out
  netsh.exe advfirewall firewall add rule name="serv" dir=out protocol=tcp localport=8080 action=allow
  echo Set tcp:8080:in
  netsh.exe advfirewall firewall add rule name="windows" dir=in protocol=tcp localport=8580 action=allow
  echo Set tcp:8080:out
  netsh.exe advfirewall firewall add rule name="windows" dir=out protocol=tcp localport=8580 action=allow
  echo Set gocr:in
  netsh.exe advfirewall firewall add rule name="gocr" dir=in program=%programdata%\Bin-Win64\gocr.exe action=allow
  echo Set pycr:out
  netsh.exe advfirewall firewall add rule name="gocr" dir=out program=%programdata%\Bin-Win64\gocr.exe action=allow
  echo Set wsr:in
  netsh.exe advfirewall firewall add rule name="wsr" dir=in program=%programdata%\Bin-Win64\wsr.exe action=allow
  echo Set wsr:out
  netsh.exe advfirewall firewall add rule name="wsr" dir=out program=%programdata%\Bin-Win64\wsr.exe action=allow
)

cd %programdata%
Path=%path%;%programdata%\Bin-Win64;
if not exist ssh\administrators_authorized_keys (
  echo Install SSH
  call:download ssh-win64Pack.exe
  7z.exe x ssh-win64Pack.exe -o%programdata%
  del ssh-win64Pack.exe
  del ssh-win64Pack.exe.md5
  cd /D %programdata%\OpenSSH-Win64
  powershell.exe -ExecutionPolicy Bypass -File install-sshd.ps1
  rem powershell.exe -ExecutionPolicy Bypass -File uninstall-sshd.ps1
  sc.exe config sshd start= auto
  net start sshd
  cd ..
)

cd %programdata%\Bin-Win64
if not exist conf (
  echo.
)
goto:eof

:download
set file=%1
if not exist !file! (
  call common !file!
) else (
  echo [*****] File:!file! is exist
)

goto:eof

:run_install
set ItemAll=1000
call:getbs bs
for /l %%i in (1,1,!ItemAll!) do (
  call:get_cols cols
  set /a cols-=8
  set /a process=!cols!*%%i/!ItemAll!
  set /a rest_i=!ItemAll!-%%i
  set /a rest=!cols!*!rest_i!/!ItemAll!
  set /a backspace=!cols!+8
  set /a percent_i=100*%%i/!ItemAll!
  set /a percent_f=%%i%%10
  set "number=!percent_i!.!percent_f!"
  set /a is_shi=!percent_i!/10
  set /a is_bai=!percent_i!/100
  set /p="["<nul
  call:show_item !process! #
  call:show_item !rest! -
  if !is_bai! equ 0 (
    if !is_shi! equ 0 (
      set /p="]  "<nul
    ) else (
      set /p="] "<nul
    )
  ) else (
    set /p="]"<nul
  )
  set /p="!number!%%"<nul
  call:sleep 1
  call:show_item_bs !backspace!
)

:for_done
echo.
pause
goto:eof

:show_item_bs
set strTemp=
for /l %%i in (1,1,%1) do (
  set "strTemp=!strTemp!%bs%"
)

set /p="!strTemp!"<nul
goto:eof

:show_item
set strTemp=
for /l %%i in (1,1,%1) do (
  set "strTemp=!strTemp!%2"
)

set /p="!strTemp!"<nul
goto:eof

:getbs
for /F %%a in ('"prompt $h&for %%b in (1) do rem"')do Set "%~1=%%a"
goto:eof

:sleep
for /l %%i in (1,1,%1) do choice /t 1 /d y /n > nul
goto:eof

:get_cols
for /f "tokens=*" %%i in ('mode ^| find "Columns:"') do (
  set gotStr="%%i"
  set gotStr=!gotStr:Columns:=!
  set gotStr=!gotStr: =!
  set gotStr=!gotStr:"=!
)

Set "%~1=!gotStr!"
rem Set "%~1=10"
goto:eof

:eof
endlocal
rem cacls *.* /t /e /c /g <whoami>:F
rem cacls *.* /t /e /c /r <whoami>
