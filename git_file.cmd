@echo off
setlocal EnableDelayedExpansion
set ThisDir=%cd%
set Name=
set FileType=
set IsFlag=0
set IsType=0
set IsWaitFlag=0
for %%i in (%*) do (
  :: 判断是否为参数
  for /f "tokens=*" %%i in ('echo %%i ^| find "-"') do (
    set IsFlag=1
  )
  if "!IsFlag!" equ "1" ( :: 如果是flag标签
    if "%%i" equ "--wait" (
      set/a IsWaitFlag+=1
    )
    if "%%i" equ "--type" (
      set IsType=1
    )
  ) else (
    if "!IsWaitFlag!" equ "1" (
      set/a IsWaitFlag+=%%i
    )
    if "!IsType!" equ "1" (
      if "!FileType!" equ "" (
        set FileType=%%i
      )
    )
    if "!Name!" equ "" (
      if "!FileType!" equ "" (
        set Name=%%i
      )
    )
  )
  set IsFlag=0
)

set FILE_DIR=C:\ProgramData\Bin-Win64

if "!FileType!" equ "" (
  echo FileType[.gitignore^|.gitattributes] is must input!
  goto:END
)

if exist !FileType! (
  echo This Dir exist !FileType! file!
  goto:END
)

if "!Name!" equ "" (
  set /p Name=Input a name:
  if "!Name!" equ "prot" (
    set Name=proteus
  ) else if "!Name!" equ "py" (
    set Name=python
  )
)

if not exist %FILE_DIR%\!Name!!FileType! (
  cd/d %FILE_DIR%
  call common !Name!!FileType!
  cd/d %ThisDir%
)

if exist %FILE_DIR%\!Name!!FileType! (
  echo Goto get %FILE_DIR%\!Name!!FileType! file...
  copy %FILE_DIR%\!Name!!FileType! !FileType!
) else (
  echo File %FILE_DIR%\!Name!!FileType! is not exist
)
goto:END

:END
set WAIT_SEC=0
if "!IsWaitFlag!" equ "0" (
  goto:eof
) else if "!IsWaitFlag!" equ "1" (
  set WAIT_SEC=9
) else (
  set/a WAIT_SEC+=!IsWaitFlag!-1
)

if %WAIT_SEC% gtr 9 (
  set WAIT_SEC=9
)
call:getbs bs
set/p="Wait to Exit: "<nul
for /l %%i in (%WAIT_SEC%,-1,0) do (
  set /p="%%i"<nul
  choice /t 1 /d y /n >nul
  set /p="%bs%"<nul
)
goto:eof

:getbs
  for /F %%a in ('"prompt $h&for %%b in (1) do rem"')do Set "%~1=%%a"
goto:eof

