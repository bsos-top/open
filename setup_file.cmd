@echo off
net session 2>nul>nul
if %errorlevel% neq 0 (
  echo [31mRequires administrator to run[0m
  pause
  goto:eof
)
cd/d %programdata%\Bin-Win64\
if exist filePack.exe (
  del filePack.exe
)
call common filePack.exe
filePack.exe

