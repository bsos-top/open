@echo off
setlocal EnableDelayedExpansion
set _GOROOT=C:\Programs\go\go1.19.3
set _GOPATH=%userprofile%\go
set _JAVA_HOME=C:\Programs\java\jdk8
set _KEIL_PATH=C:\Keil_v5
set _QT_DIR=C:\Programs\qt\Qt5.12.12\5.12.12
set _QT_MINGW73X86_CMAKE_PREFIX_PATH=%_QT_DIR%\mingw73\lib\cmake
set _QT_MINGW73X64_CMAKE_PREFIX_PATH=%_QT_DIR%\mingw73_64\lib\cmake
set _QT_MSVC2017X86_CMAKE_PREFIX_PATH=%_QT_DIR%\msvc2017\lib\cmake
set _QT_MSVC2017X64_CMAKE_PREFIX_PATH=%_QT_DIR%\msvc2017_64\lib\cmake
set errno=0
net session 2>nul>nul
if %errorlevel% neq 0 (
  echo [31mRequires administrator to run[0m
  pause
  goto:eof
)

if "%1" == "" (
  echo [31mError[0m:Require a param[--all^|mingw^|python37^|go^|keil]
  call:return 1
  goto:eof
)

if "%1" == "--all" (
  call:senv_go
  call:senv_java
  call:senv_qt
)
if "%1" == "java" (
  goto:senv_java
)
if "%1" == "go" (
  goto:senv_go
)
if "%1" == "qt" (
  goto:senv_qt
)
echo [31mError[0m:Param^([32m%1[0m^) is not support
goto:eof

::==============
:: Function
::==============
::=== go start
:senv_go
echo %GOROOT% | findstr/c:"go" >nul && (
  goto:end_env_add_goroot
)
setx GOROOT %_GOROOT%
:end_env_add_goroot
echo %GOPATH% | findstr/c:"go" >nul && (
  goto:end_env_add_gopath
)
if not exist %_GOPATH% (
  mkdir %_GOPATH%
)
setx GOPATH %_GOPATH%
:end_env_add_gopath
goto:eof
::=== go end
::--------------
::=== java start
:senv_java
echo %JAVA_HOME% | findstr/c:"java" >nul && (
  goto:end_env_add_java
)
setx JAVA_HOME %_JAVA_HOME%
:end_env_add_java
goto:eof
::=== java end
::--------------
::=== qt start
:senv_qt
echo %QT_DIR% | findstr/c:"qt" >nul && (
  goto:end_env_add_qt
)
if not exist %_QT_DIR% (
  mkdir %_QT_DIR%
)
setx QT_DIR %_QT_DIR%
setx QT_MINGW73X86_CMAKE_PREFIX_PATH %_QT_MINGW73X86_CMAKE_PREFIX_PATH%
setx QT_MINGW73X64_CMAKE_PREFIX_PATH %_QT_MINGW73X64_CMAKE_PREFIX_PATH%
setx QT_MSVC2017X86_CMAKE_PREFIX_PATH %_QT_MSVC2017X86_CMAKE_PREFIX_PATH%
setx QT_MSVC2017X64_CMAKE_PREFIX_PATH %_QT_MSVC2017X64_CMAKE_PREFIX_PATH%
:end_env_add_qt
goto:eof
::=== qt end
::==============

:return
set errno=%1
:eof
exit/b %errno%

