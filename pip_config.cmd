@echo off
:: pip -v config list
pip config --site set global.index-url http://pypi.tuna.tsinghua.edu.cn/simple
pip config --site set install.trusted-host pypi.tuna.tsinghua.edu.cn

:: pip config --site set global.index-url http://mirrors.aliyun.com/pypi/simple
:: pip config --site set install.trusted-host mirrors.aliyun.com

python.exe -m pip install --upgrade pip
pip install readchar
pip install requests
pip install requests_toolbelt
pip install scapy
