@echo off
setlocal EnableDelayedExpansion

set ThisDir=%cd%
set ErrorRetuen=1

if exist D: (
  call:new_dir D:\XunLei\YunPan
  call:new_dir D:\XunLei\Download
) else (
  echo D:\ NotExist
  goto:eof
)

call:check_file ThunderNetworkProfiles.exe
call:check_file ThunderNetworkProgram.exe
call:check_file ThunderNetworkResources.exe

call senv.cmd
call cdps.cmd
if not %errorlevel% == 0 (
  goto:return
)
echo Now In %cd%
if exist ThunderNetwork (
  echo %cd%\ThunderNetwork is exist, now exit
  goto:return
)
%ThisDir%\ThunderNetworkProfiles.exe
%ThisDir%\ThunderNetworkProgram.exe
%ThisDir%\ThunderNetworkResources.exe
set ErrorRetuen=0
goto:return

:: FunctionStart
:new_dir
if not exist "%1" (
  mkdir "%1"
)
goto:eof
:check_file
if not exist "%1" (
  echo %1 NotExist
  goto:return
)
goto:eof

:return
cd/d %ThisDir%
if not %ErrorRetuen% == 0 (
  pause
)
exit/b %ErrorRetuen%

