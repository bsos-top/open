@echo off
chcp 65001 > nul
setlocal EnableDelayedExpansion
set DIR=%cd%
set ARG=%1

set min=1000
set max=9999
set/a mod=%max% - %min% + 1
set/a rand_int=%random%%%%mod% + %min%

set sfxFile=7zCon
if "%ARG%" == "gui" (
  set sfxFile=7z
)

set is_download=0
:beginfor
if not %is_download% == 0 (
  goto:eof
)
for /f "tokens=*" %%i in ('where %sfxFile%.sfx 2^> nul') do (
  set sfxDir=%%i
  goto:endfor
)
:endfor

echo !sfxDir! | findstr/c:"%sfxFile%.sfx" > nul && (
  echo SFX File^([32m%sfxFile%.sfx[m^) is exist
) || (
  echo SFX File^([32m%sfxFile%.sfx[m^) is not exist
  echo Now download
  cd/d %programdata%\Bin-Win64
  call common.cmd 7zPack.exe
  7zPack.exe
  set is_download=1
  cd/d %DIR%
  goto:beginfor
)

:run_start
cls
echo.
echo Please drag a file or folder to this window!
echo.
set /p flag_input=^>^>^> 
set flag=
if "%flag_input%" == "exit" (
  goto:eof
)
if "%flag_input%" == "ls" (
  dir
  pause
  goto:run_start
)

set is_clink=0
echo "%flag_input%" | findstr/c:"clink\clink_x64.exe" >nul && (
  set is_clink=1
)

if %is_clink% == 1 (
  set flag=%DIR%\clink
) else (
  echo "%flag_input%" | findstr/c:":" >nul && (
    set flag=%flag_input%
  ) || (
    set flag=%DIR%\%flag_input%
  )
)

echo Input:%flag%
call:print %flag%
echo Done
goto:eof

:print
echo In Progress...
if "%1" == "" (
  echo Input null
  goto:eof
)
if not exist %1 (
  echo File[%1] is not exist
  goto:eof
)

:: a/b/c.txt
:: %~n1 c
:: %~nx1 c.txt
:: TODO check file path exist space
7z a %~dp1%~nx1_%rand_int%.7z %flag%
:: echo !sfxDir!+%~dp1%~nx1_%rand_int%.7z to %~dp1%~nx1.exe
:: copy/b/y !sfxDir!+%~dp1%~nx1_%rand_int%.7z %~dp1%~nx1.7z
:: copy/b/y !sfxDir!+%~dp1%~nx1_%rand_int%.7z %~dp1%~nx1.7z
:: del %~dp1%~nx1_%rand_int%.7z
goto:eof

