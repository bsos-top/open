@echo off

mkdir C:\Programs\nodejs
cd/d C:\Programs\nodejs
if not exist node-v16.14.2 (
  curl --ssl-no-revoke -L https://www.winstonkenny.cn/d/8TBgvGXS -o node-v16.14.2-win-x64.exe
  node-v16.14.2-win-x64.exe
  del node-v16.14.2-win-x64.exe
) else (
  echo node-v16.14.2 exist
  pause
  if not exist node-v16.14.2\cnpm (
    node-v16.14.2\npm install -g cnpm
  )
  if not exist node-v16.14.2\vue (
    Path=%Path%;C:\Programs\nodejs\node-v16.14.2;
    cnpm install -g @vue/cli@5.0.4
  )
)

exit/b

