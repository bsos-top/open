@echo off

set _DIR_=%cd%
cd/d "%programdata%\Bin-Win64"
if not "%cd%" == "%programdata%\Bin-Win64" (
  echo Change Dir Error
  echo ThisDir:%cd%
  goto:eof
)

if exist clink ( rd/s/q clink)
if exist clink.exe ( del clink.exe)

call common clink.exe
clink.exe
del clink.exe

cd clink
call common clink_completion.lua
call common clink_prompt.lua
cd ..

cd/d %_DIR_%

