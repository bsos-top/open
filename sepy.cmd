@echo off
if "%path:~-1%" == ";" (
  goto:end_path_add_
)
path=%path%;
:end_path_add_

echo %path% | findstr/c:"C:\\Programs\\python\\Python37;" >nul && (
  goto:end_path_add_python3
)
path=%path%C:\Programs\python\Python37;
:end_path_add_python3

echo %path% | findstr/c:"C:\\Programs\\python\\Python37\\Scripts;" >nul && (
  goto:end_path_add_python3_scripts
)
path=%path%C:\Programs\python\Python37\Scripts;
:end_path_add_python3_scripts

