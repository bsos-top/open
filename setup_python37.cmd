@echo off
title BsWinSetup
:: utf-8 65001 en 437 zh 936
chcp 65001 > nul
if "%OS%"=="Windows_NT" (
  setlocal EnableDelayedExpansion
) else (
  echo OS is not Windows_NT
  pause
  goto:eof
)
set OLD_DIR=%cd%

mkdir C:\Programs\python
cd C:\Programs\python
call:download Python37.exe
cd C:\Programs\python
if not exist Python37 (
  7z.exe x Python37.exe
)

Python37\Scripts\pip.exe config --site set global.index-url http://pypi.tuna.tsinghua.edu.cn/simple
Python37\Scripts\pip.exe config --site set install.trusted-host pypi.tuna.tsinghua.edu.cn
rem Python37\Scripts\pip.exe config --site set global.index-url http://mirrors.aliyun.com/pypi/simple/
rem Python37\Scripts\pip.exe config --site set install.trusted-host mirrors.aliyun.com

del Python37.exe
del Python37.exe.md5
goto:eof

:download
set file=%1
if not exist !file! (
  call common.cmd !file!
) else (
  echo [*****] File:!file! is exist
)
goto:eof

:eof
exit/b
