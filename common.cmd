@echo off
set DownloadUrl="http://gitee.com/bsos-top/open/raw/master/"

if "%OS%"=="Windows_NT" (
  setlocal EnableDelayedExpansion
) else (
  echo OS is not Windows_NT
  goto:eof
)
set _VERSION_=1.0.1

if "%1"=="" (
  set FileName=%~n0
  call:log warn "You must enter a file like abc.txt"
  goto:eof
) else (
  if "%1"=="-v" (
    echo Version: %_VERSION_%
    goto:eof
  )
  if "%1"=="--version" (
    echo Version: %_VERSION_%
    goto:eof
  )
  set FileName=%1
)

if exist "%FileName%" (
  del %FileName%
  call:log info "File is exist is deleted! errorlevel=%errorlevel%"
)

call:log start "File:%FileName%"
set IsWget=
if exist %programdata%\Bin-Win64\wget.exe (
  set IsWget="true"
) else (
  set IsWget="false"
)

set NeedUnix2Dos=0
if "%FileName:~-4%" == ".cmd" ( set NeedUnix2Dos=1 )
if "%FileName:~-4%" == ".bat" ( set NeedUnix2Dos=1 )
if "%FileName:~-4%" == ".vbs" ( set NeedUnix2Dos=1 )

if %NeedUnix2Dos% == 1 (
  call:log info "Need unix2dos.exe for *%FileName:~-4%"
  if not exist %programdata%\Bin-Win64\unix2dos.exe (
    call:log error "No unix2dos.exe in Path"
    goto:eof
  )
)

if %IsWget% equ "true" (
  %programdata%\Bin-Win64\wget.exe --no-check-certificate -qN %DownloadUrl%%FileName%
) else (
  curl --ssl-no-revoke -sfLO %DownloadUrl%%FileName%
)

if %errorlevel% neq 0 (
  call:log info "File:%FileName% Is Not Found"
  call:log info "Goto:Download By For"
  goto:DOWNLOAD_FOR
) else (
  call:log info "File:%FileName% Done"
  for /f %%a in (%FileName%) do (
    if "%%a"=="<DOCTYPE" (
      call:log info "File:%FileName% Is HTML"
      del %FileName%
      call:log info "Goto:Download By For"
      goto:DOWNLOAD_FOR
    ) else (
      goto:eof
    )
  )
  goto:eof
)
goto:eof

:DOWNLOAD_FOR
SET /a count=99
for /l %%i in (0,1,%count%) do (
  if %%i lss 10 (
    if %IsWget% equ "true" (
      %programdata%\Bin-Win64\wget.exe --no-check-certificate -qN %DownloadUrl%%FileName%0%%i
    ) else (
      curl --ssl-no-revoke -sfLO %DownloadUrl%%FileName%0%%i
    )
    if !errorlevel! neq 0 (
      if %%i lss 1 (
        call:log error "File:%FileName%0%%i Is Not Found"
        goto:ERROR
      ) else (
        call:log info "File:%FileName%0%%i Is Not Found"
        goto:DOWNOK
      )
    ) else (
      call:log info "File:%FileName%0%%i Done"
      call set "Myvar=%%Myvar%%+%FileName%0%%i"
      call set "Mydel=%%Mydel%% %FileName%0%%i"
    )
  ) else (
    if %IsWget% equ "true" (
      %programdata%\Bin-Win64\wget.exe --no-check-certificate -qN %DownloadUrl%%FileName%%%i
    ) else (
      curl -sfLO %DownloadUrl%%FileName%%%i --ssl-no-revoke
    )
    if !errorlevel! neq 0 (
      call:log info "File:%FileName%%%i Is Not Found"
      goto DOWNOK
    ) else (
      call:log info "File:%FileName%%%i Done"
      call set "Myvar=%%Myvar%%+%FileName%%%i"
      call set "Mydel=%%Mydel%% %FileName%%%i"
    )
  )
)
goto:eof

:DOWNOK
call:log info "Download All Done"
copy /b /y %Myvar:~1% %FileName% > nul

set /a f_count=0
for /f "tokens=*" %%i in ('certutil -hashfile %FileName% MD5') do (
  set /a f_count+=1
  if "!f_count!" == "2" (
    set gotMD5Str=%%i
    goto:end_for1
  )
)
:end_for1
set gotMD5Str=!gotMD5Str: =!
call:log info "!gotMD5Str!"

if %IsWget% equ "true" (
  %programdata%\Bin-Win64\wget -qN %DownloadUrl%%FileName%.md5
) else (
  curl -sfLO %DownloadUrl%%FileName%.md5 --ssl-no-revoke
)

if %errorlevel% neq 0 (
  call:log error "No MD5 file download"
) else (
  call:log info "MD5 File Download:"
  set /a f_count=0
  :beginfor_type_md5
  for /f "tokens=*" %%i in ('type %FileName%.md5') do (
    set /a f_count+=1
    if "!f_count!" == "1" (
      set fileMD5Str=%%i
      goto:endfor_type_md5
    )
  )
  :endfor_type_md5
  call:log info "!fileMD5Str!"
  if "!fileMD5Str!" neq "!gotMD5Str!" (
    call:log error "MD5 error"
    pause
  )
)

del %Mydel:~1%
if %NeedUnix2Dos% == 1 (
  %programdata%\Bin-Win64\unix2dos.exe -f %FileName%
)
:beginfor_whoami
set var_whoami=
for /f "tokens=*" %%i in ('whoami') do (
  set var_whoami=%%i
)
:endfor_whoami
cacls %FileName% /t /e /c /g %var_whoami%:F
goto:eof

:ERROR
call:log error "Download ERROR!!!"
goto:eof

:log
set typ=%1
set str=%2
set "str=%str:~1,-1%"
if %typ% equ start (
  echo [[32mStart[0m] %str%
) else if %typ% equ info (
  echo [[37m Info[0m] %str%
) else if %typ% equ warn (
  echo [[33m Warn[0m] %str%
) else if %typ% equ error (
  echo [[31mError[0m] %str%
) else (
  echo [     ] %str%
)
goto:eof

:return
exit/b %1
