@echo off
if "%path:~-1%" == ";" (
  goto:end_path_add_
)
path=%path%;
:end_path_add_

echo %path% | findstr/c:"%USERPROFILE%\\anaconda3;" >nul && (
  goto:end_path_add_anaconda
)
path=%path%%USERPROFILE%\anaconda3;
:end_path_add_anaconda

echo %path% | findstr/c:"%USERPROFILE%\\anaconda3\\Scripts;" >nul && (
  goto:end_path_add_anaconda_scripts
)
path=%path%%USERPROFILE%\anaconda3\Scripts;
:end_path_add_anaconda_scripts

