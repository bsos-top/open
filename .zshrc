export ZSH="$HOME/.oh-my-zsh"
ZSH_THEME="ys"
DISABLE_AUTO_UPDATE="true"
DISABLE_UPDATE_PROMPT="true"

plugins=(
git
sudo
)

source $ZSH/oh-my-zsh.sh
_result1=$(echo $PATH | grep :$HOME/.shell$)
_result2=$(echo $PATH | grep :$HOME/.shell:)
if [[ -z $_result1 && -z $_result2 ]];then
  export PATH="$PATH:$HOME/.shell"
fi

_shcp_complete_func() {
  local HOST cur opts cmd
  COMPREPLY=()
  cur="${COMP_WORDS[COMP_CWORD]}"
  now="${COMP_LINE##* }"
  this_dir=""
  cmd="${COMP_WORDS[COMP_CWORD-1]}"

  HOST=$(grep -v '^#' /etc/hosts | cut -d ' ' -f2-)
  # check $HOME/.ssh/config
  if [ -f $HOME/.ssh/config ];then
    HOST="$HOST $(grep '^Host ' $HOME/.ssh/config | cut -d ' ' -f2-)"
  else
    touch $HOME/.ssh/config
    chmod 600 $HOME/.ssh/config
  fi
  case "${cmd}" in
  "shcp" )
    opts="$HOST -u -d"
  ;;
  "-u" )
    opts=$(ls -a)
  ;;
  esac
  COMPREPLY=( $( compgen -W "$opts" -- "$cur" ) )
  return 0
}
complete -o . -F _shcp_complete_func shcp
setopt autolist
unsetopt menucomplete

if [ -e ~/.env ];then
  source ~/.env
fi

