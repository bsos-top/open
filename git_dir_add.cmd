@echo off
::chcp 437 > nul
for /f "delims=" %%i in ('git status 2^>^&1 ^| find "not a git repository"') do (
  echo This dir is not a repository
  goto:eof
)
set NeedInsert=%cd%
if "%NeedInsert%" equ "" (
  echo Need a git repo url
  goto:eof
)
for /f %%i in ('type %USERPROFILE%\.gitdir 2^>nul') do (
  if "%NeedInsert%" equ "%%i" (
    echo Git[%NeedInsert%] is exist in .gitdir
    goto:eof
  )
)
echo %cd%>> %USERPROFILE%\.gitdir

