let g:os_is_win=0
if(has("win32") || has("win64") || has("win95") || has("win16"))
  let g:os_is_win=1
endif

" ===
" === Auto load for first time uses
" ===
" Auto install vim-plug
if os_is_win == 1
  if empty(glob($USERPROFILE.'/vimfiles/autoload/plug.vim'))
    silent !curl -sfLo \%USERPROFILE\%/vimfiles/autoload/plug.vim --create-dirs
          \ https://gitee.com/bsos-top/open/raw/master/plug.vim
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
  endif
else
  if empty(glob('~/.vim/autoload/plug.vim'))
    silent !curl -sfLo $HOME/.vim/autoload/plug.vim --create-dirs
          \ https://gitee.com/bsos-top/open/raw/master/plug.vim
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
  endif
endif

" ===
" === Create a _machine_specific.vim file to adjust machine specific stuff
" ===
if os_is_win == 1
  if empty(glob($USERPROFILE.'/vimfiles/_machine_specific.vim'))
    if empty(glob($USERPROFILE.'/vimfiles/default_configs/_machine_specific_default.vim'))
      silent !curl -sfLo \%USERPROFILE\%/vimfiles/default_configs/_machine_specific_default.vim --create-dirs
            \ https://gitee.com/bsos-top/open/raw/master/_machine_specific_default_win.vim
    endif
    silent !cmd/c "copy/y \%USERPROFILE\%\vimfiles\default_configs\_machine_specific_default.vim \%USERPROFILE\%\vimfiles\_machine_specific.vim > nul"
  endif
  source $HOME/vimfiles/_machine_specific.vim
else
  if empty(glob('~/.vim/_machine_specific.vim'))
    if empty(glob('~/.vim/default_configs/_machine_specific_default.vim'))
      silent !curl -sfLo $HOME/.vim/default_configs/_machine_specific_default.vim --create-dirs
            \ https://gitee.com/bsos-top/open/raw/master/_machine_specific_default.vim
    endif
    silent!exec "!cp ~/.vim/default_configs/_machine_specific_default.vim ~/.vim/_machine_specific.vim"
  endif
  source $HOME/.vim/_machine_specific.vim
endif

" ====================
" === Editor Setup ===
" ====================
" ===
" === System
" ===
syntax on
filetype on
"let &t_ut=''

" ===
" === Editor benavior
" ===
set backspace=indent,eol,start
set encoding=utf-8
set fileencodings=utf-8,utf-16le,gbk,gb18030,gb2312,big5
set list
set listchars=tab:\|\ ,trail:▫
"let &t_SI = "\<Esc>]50;CursorShape=1\x7" " Not apply in vim
"let &t_SR = "\<Esc>]50;CursorShape=2\x7"
"let &t_EI = "\<Esc>]50;CursorShape=0\x7"
set laststatus=2
set autochdir
set scrolloff=5

set nonumber
set tabstop=2
set shiftwidth=2
set softtabstop=2
set smarttab
set autoindent
"set paste
set expandtab
set mouse=
"set bin noeol

" === Auto run when touch or open
au BufNewFile,BufRead * set fileformat=unix
au BufNewFile,BufRead *.cmd,*.bat,*.vbs,*.reg,*.ps1,*.psd1,*.psm1 set fileformat=dos
au BufNewFile,BufRead .gitconfig,*.go set noexpandtab
au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif

" === Config undofile
if os_is_win == 1
  if empty(glob($USERPROFILE.'/vimfiles/tmp/backup'))
    silent !mkdir \%USERPROFILE\%\vimfiles\tmp\backup
  endif
  if empty(glob($USERPROFILE.'/vimfiles/tmp/undo'))
    silent !mkdir \%USERPROFILE\%\vimfiles\tmp\undo
  endif
  set backupdir=$HOME/vimfiles/tmp/backup,.
  set directory=$HOME/vimfiles/tmp/backup,.
  if has('persistent_undo')
      set undofile
      set undodir=$HOME/vimfiles/tmp/undo,.
  endif
else
  silent !mkdir -p $HOME/.vim/tmp/backup
  silent !mkdir -p $HOME/.vim/tmp/undo
  set backupdir=$HOME/.vim/tmp/backup,.
  set directory=$HOME/.vim/tmp/backup,.
  if has('persistent_undo')
      set undofile
      set undodir=$HOME/.vim/tmp/undo,.
  endif
endif

" ===
" === Basic Mappings
" ===
let mapleader=" "
noremap = nzz
noremap - Nzz
noremap <LEADER><CR> :noh
noremap Q @@

"nnoremap <LEADER>rc :e $HOME/.vimrc<CR>
nnoremap R :source $MYVIMRC<CR>
nnoremap Q :q<CR>
nnoremap S :w<CR>

inoremap <C-J><C-K> <esc>
vnoremap <C-J><C-K> <esc>
" Copy to system clipboard
vnoremap Y "+y
vnoremap <C-K> 5k
vnoremap <C-J> 5j
nnoremap <C-K> 5k
nnoremap <C-J> 5j
nnoremap W 5w
nnoremap B 5b
nnoremap <C-U> 5<C-y>
nnoremap <C-E> 5<C-e>
" ===
" === Command Mod Cursor Movement
" ===
cnoremap <C-a> <Home>
cnoremap <C-e> <End>
cnoremap <C-p> <Up>
cnoremap <C-n> <Down>
cnoremap <C-b> <Left>
cnoremap <C-f> <Right>
" ===
" === Window management
" ===
" Use <space> + new arrow keys for moving the cursor around windows
noremap <LEADER>w <C-w>w
noremap <LEADER>k <C-w>k
noremap <LEADER>j <C-w>j
noremap <LEADER>h <C-w>h
noremap <LEADER>l <C-w>l
noremap qf <C-w>o

" Disable the default s key
noremap s <nop>
" split the screens to up (horizontal), down (horizontal), left (vertical), right (vertical)
noremap sk :set nosplitbelow<CR>:split<CR>:set splitbelow<CR>
noremap sj :set splitbelow<CR>:split<CR>
noremap sh :set nosplitright<CR>:vsplit<CR>:set splitright<CR>
noremap sl :set splitright<CR>:vsplit<CR>
noremap ssv <C-w>t<C-w>H
noremap ssh <C-w>t<C-w>K
" Resize splits with arrow keys
"noremap <up> :res +5<CR>
"noremap <down> :res -5<CR>
"noremap <left> :vertical resize-5<CR>
"noremap <right> :vertical resize+5<CR>

noremap t <nop>
noremap te :tabe<CR>
noremap tn :tabnext<CR>
noremap tp :tabprevious<CR>
noremap th :-tabnext<CR>
noremap tl :+tabnext<CR>
" Move the tabs with tmn and tmi
noremap tmh :-tabmove<CR>
noremap tml :+tabmove<CR>

"curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://gitee.com/bsos-top/open/raw/master/plug.vim
"curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
call plug#begin()
"Plug 'mg979/vim-visual-multi', {'branch': 'master'}
Plug 'https://gitee.com/bsos-top/vim-visual-multi.git', {'branch': 'master'} " vim-visual-multi
"Plug 'scrooloose/nerdtree', { 'on': 'NERDTreeToggle' }
"Plug 'Valloric/YouCompleteMe'
call plug#end()

" ===
" === NERDTreeToggle
" ===
noremap f <nop>
nmap ff :NERDTreeToggle<CR>

