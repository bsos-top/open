@echo off
chcp 65001 > nul
::chcp 437 > nul :: en
::chcp 936 > nul :: zh
if "%OS%" neq "Windows_NT" (
  echo OS is not Windows_NT
  pause
  goto:eof
)

:: BatchFile not support path=%path%; in ()
::if not "%path_last%" == ";" (
::  path=%path%;
::)
if "%path:~-1%" == ";" (
  goto:end_path_add_
)
path=%path%;
:end_path_add_

echo %path% | findstr/c:"C:\\Qt\\Qt5.12.12\\5.12.12\\mingw73_64\\bin;" >nul && (
  goto:end_path_add_qt1
)
path=%path%C:\Qt\Qt5.12.12\5.12.12\mingw73_64\bin;
:end_path_add_qt1

echo %path% | findstr/c:"C:\\Qt\\Qt5.12.12\\Tools\\mingw730_64\\bin;" >nul && (
  goto:end_path_add_qt2
)
path=%path%C:\Qt\Qt5.12.12\Tools\mingw730_64\bin;
:end_path_add_qt2

