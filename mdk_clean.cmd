@echo off
if exist *.uvproj (
	del *.uvgui.* 2>nul
	del *.uvopt 2>nul
	rd/s/q Objects Listings 2>nul
)
if exist *.uvprojx (
	del *.uvguix.* 2>nul
	del *.uvoptx 2>nul
	del *.lst 2>nul
	rd/s/q DebugConfig mdk RTE 2>nul
)
del build_log.txt 2>nul

:END
if "%1" neq "wait" (
	goto:eof
)
set WAIT_SEC=%2
if "%WAIT_SEC%" equ "" (
	set WAIT_SEC=9
)
if %WAIT_SEC% gtr 9 (
	set WAIT_SEC=9
)
call:getbs bs
set/p="Wait to Exit: "<nul
for /l %%i in (%WAIT_SEC%,-1,0) do (
	set /p="%%i"<nul
	choice /t 1 /d y /n >nul
	set /p="%bs%"<nul
)
goto:eof

:getbs
	for /F %%a in ('"prompt $h&for %%b in (1) do rem"')do Set "%~1=%%a"
goto:eof
