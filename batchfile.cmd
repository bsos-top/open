@echo off
:: ==================================
:: - batchfile for windows
:: ==================================
set "error=1"
set "old_dir=%cd%"
set "script_path=%~dp0"
if "%script_path:~-1%"=="\" set "script_path=%script_path:~0,-1%"
cd /d "%script_path%"
if exist ".env.cmd" (
  call .env.cmd
)
:: ==================================
:: core process start

:: core process end
:: ==================================
:eof
set "error=0"
:return
cd "%old_dir%"
pause
exit /b %error%
