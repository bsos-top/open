#!/usr/bin/env python3
import random
import string
import argparse

def generate_password(length=8):
    if length < 8:
        print("Password length should be at least 8.")
        return

    # 至少包含两个大小写字母、两个数字和两个特殊字符
    min_chars = 2
    min_lower = 2
    min_upper = 2
    min_digits = 2
    min_special = 2

    # 随机生成密码
    password = ''.join(random.choices(string.ascii_lowercase, k=min_lower))
    password += ''.join(random.choices(string.ascii_uppercase, k=min_upper))
    password += ''.join(random.choices(string.digits, k=min_digits))
    password += ''.join(random.choices(".,-!@", k=min_special))
    
    remaining_length = length - min_chars * 4
    
    if remaining_length < 0:
        remaining_length = 0
    
    password += ''.join(random.choices(string.ascii_letters + string.digits + ".,-!@", k=remaining_length))

    # 将密码中的字符顺序打乱
    password_list = list(password)
    random.shuffle(password_list)
    return ''.join(password_list)

def main():
    parser = argparse.ArgumentParser(description='Generate a random password.')
    parser.add_argument('-l', '--length', type=int, default=8, help='Length of the password')
    args = parser.parse_args()
    
    password = generate_password(args.length)
    print("Generated Password:", password)

if __name__ == "__main__":
    main()
#import string
#import random
#ran_str = ''.join(random.sample(string.ascii_letters+string.digits,8))
#print(ran_str)

