@echo off
:Ping
:: Delay 5s
choice /t 5 /d y /n > nul
ping -n 3 -w 2000 www.baidu.com > nul
:: 0 yes 1 no
IF ERRORLEVEL 1 goto Ping
IF ERRORLEVEL 0 goto Start1
:Start1
cd %programdata%\Bin-Win64

for /F %%i in ('date /T') do ( set date_=%%i)
for /F %%i in ('time /T') do ( set time_=%%i)
echo %date_% %time_% >> run.log

if exist gocr.exe (
    start /b gocr.exe
)

if exist wsr.exe_ (
    move/y wsr.exe_ wsr.exe
)

if exist wsr.exe (
    start /b wsr.exe
)

