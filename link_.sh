#!/usr/bin/sh
mkdir -p ~/.shell

__FILETAG__=$1

if [ -z $__FILETAG__ ];then
  read -p "Please input a file tag:" __FILETAG__
  if [ -z $__FILETAG__ ];then
    echo You must input a file tag
    exit 1
  fi
fi

function _link {
  local _FILE_=$1
#  local _FILE_PATH_=$2
  local _FILE_TARGET_=$2
#  if [ -n "${_FILE_PATH_}" ];then
#    if [ "${_FILE_PATH_##*/}" == "" ];then
#      _PATH_=$_FILE_PATH_/
#    elif [ "${_FILE_PATH_%%/*}" == "" ];then
#      printf "You must enter the relative path to your HOME directory"
#      exit 1
#    fi
#  fi
  if [ -z $_FILE_TARGET_ ];then
    _FILE_TARGET_=${_FILE_}
  fi

  case ${_FILE_} in
    .vimrc|.tmux.conf|.bashrc|.zshrc)
      _FILE_PATH_=
      ;;
    *)
      case ${_FILE_##*.} in
        sh|py)
          if [ "$_FILE_TARGET_" == "${_FILE_}" ];then
            _FILE_TARGET_=${_FILE_%.*}
          fi
          _FILE_PATH_=.shell/
          ;;
        *)
          ;;
      esac
      ;;
  esac

  if [ -e `pwd`/${_FILE_} ];then
    ln -fs `pwd`/${_FILE_} ~/${_FILE_PATH_}${_FILE_TARGET_}
    printf "Linked File(\033[34m%s\033[m -> \033[32m%s\033[m)\n" "~/${_FILE_PATH_}${_FILE_TARGET_}" `pwd`/${_FILE_}
  else
    printf "\033[31mError: File(\033[32m%s\033[31m) Is Not Exist\033[m\n" ./${_FILE_}
  fi
}

function link_vim {
  _link .vimrc
}

function link_tmux {
  _link .tmux.conf
}

function link_zsh {
  _link .zshrc
}

function link_bash {
  _link .bashrc
}

function link_shcp {
  _link shcp.sh shcp
}

function link_git {
  _link git_push.sh gpsh
  _link git_pull.sh gpll
  _link git_push.sh
  _link git_pull.sh
  _link git_status.sh gst
  _link git_del_commit_history.sh
}

function link_setup {
  _link r_.sh r_
  _link complete_make complete_make
  _link complete_shcp complete_shcp
}

case $__FILETAG__ in
  --all) link_setup;link_vim;link_shcp;link_tmux;link_zsh;link_bash;link_git;;
  setup) link_setup ;;
  shcp)  link_shcp  ;;
  vim)   link_vim   ;;
  tmux)  link_tmux  ;;
  zsh)   link_zsh   ;;
  bash)  link_bash  ;;
  git)   link_git   ;;
  *)     _link $__FILETAG__ ;;
esac

