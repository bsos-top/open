#!/usr/bin/env bash

DOCKER_DIR=`pwd`
DOCKER_NAME=${DOCKER_DIR##*/}

case $1 in
  delete)
    docker stop $DOCKER_NAME
    docker rm $DOCKER_NAME
    if [ -f docker_del.sh ];then
      chmod u+x docker_del.sh
      ./docker_del.sh $DOCKER_NAME $DOCKER_DIR
    fi
    exit 0 ;;
  logs|logs)
    docker logs $DOCKER_NAME
    exit 0 ;;
  exec)
    docker exec -it $DOCKER_NAME sh
    exit 0 ;;
  restart)
    docker restart $DOCKER_NAME
    exit 0 ;;
  images)
    docker images
    exit 0 ;;
  ps)
    docker ps --format "table {{.Names}} \t {{.Status}} \t {{.Ports}}"
    exit 0 ;;
  psa)
    docker ps -a
    exit 0 ;;
  edit)
    vim $0
    echo Edit success
    exit 0 ;;
esac

if [ -f docker_run.sh ];then
  chmod u+x docker_run.sh
else
  echo No docker_run.sh file in this dir
  exit 1
fi

docker ps -a --format "table {{.Names}}" | grep -v NAMES | grep ^$DOCKER_NAME$ > /dev/null
if [[ $? != 0 ]];then
  ./docker_run.sh $DOCKER_NAME $DOCKER_DIR
fi

