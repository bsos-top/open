#!/usr/bin/env bash

if [ -z "$1" ]; then
  echo "The first argument is empty."
  exit 1
fi

echo -e "RunCmd(\033[35m$@\033[0m)"
for dir in */; do
  if [ -d "$dir" ]; then
    cd "$dir"
    echo -e "\033[32mNowIn\033[m(\033[34m$dir\033[0m)"
    $@
    if [ $? -ne 0 ]; then
      exit 1
    fi
    cd ..
  fi
done
