一、Windows系统下查看进程、端口、资源情况
1、打开cmd
（1）查看所有正在运行的进程：netstat -ano
（2）查看指定端口号XXXX的进程：netstat –ano | findstr "XXXX"
（3）查看PID进程的具体信息：tasklist | findstr "PID"
（4）查看进程列表、内存使用：tasklist
（5）根据PID“XXXX”结束进程：taskkill / pid XXXX
（6）根据进程名结束进程：taskkill /f /t /im <pid>
（7）查看本地计算机共享资源：net share 
（8）查看内存使用情况：打开msinfo32------>打开cmd----->输入systeminfo
（9）查看cpu使用率：wmic cpu

set/?
set path
- 永久设置 GIT_HOME 变量为 abc
setx /m GIT_HOME abc

- 将 C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\bin 追加到 PATH 变量
setx /m PATH "%PATH%;"

