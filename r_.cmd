@echo off
if "%1" == "" (
  echo [31mYou must input a command[0m
  exit/b
)

for /d %%i in (*) do (
  setlocal enabledelayedexpansion
  cd "%%i"
  echo [32mNowIn[m^([34m%%i^[0m^)Run:[33m%*[0m
  call %*
  if !errorlevel! neq 0 (
    exit/b
  )
  cd ..
  endlocal
)
