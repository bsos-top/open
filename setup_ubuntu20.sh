#!/bin/bash
_user=$(whoami)
echo ThisUser:$_user
if [[ "$_user" == "root" ]]; then
	if [[ -f "/etc/apt/sources.list.bak" ]]; then
		echo /etc/apt/sources.list.bak exist
	else
		cp /etc/apt/sources.list /etc/apt/sources.list.bak
	fi
	if [[ -z $(cat /etc/apt/sources.list | grep 'mirrors.tuna.tsinghua.edu.cn') ]]; then
cat <<EOF > /etc/apt/sources.list
deb http://mirrors.tuna.tsinghua.edu.cn/ubuntu/ focal main restricted universe multiverse
deb http://mirrors.tuna.tsinghua.edu.cn/ubuntu/ focal-security main restricted universe multiverse
deb http://mirrors.tuna.tsinghua.edu.cn/ubuntu/ focal-updates main restricted universe multiverse
deb http://mirrors.tuna.tsinghua.edu.cn/ubuntu/ focal-proposed main restricted universe multiverse
deb http://mirrors.tuna.tsinghua.edu.cn/ubuntu/ focal-backports main restricted universe multiverse
EOF
	else
		echo /etc/apt/sources.list exist tsinghua.edu
	fi
	apt-get update
	apt-get install -y zsh
else
	cd
	if [[ -z $(echo $SHELL | grep '/bin/zsh') ]]; then
		echo Change Shell to /bin/zsh
		chsh -s /bin/zsh
	fi
	if [[ -d ".oh-my-zsh" ]]; then
		echo .oh-my-zsh exist
	else
		curl -LO gitee.com/bsos-top/open/raw/master/oh-my-zsh.tar.gz
		tar xzf oh-my-zsh.tar.gz
		rm oh-my-zsh.tar.gz
	fi
	if [[ -f ".zshrc" ]]; then
		echo .zshrc exist
	else
		curl -LO gitee.com/bsos-top/open/raw/master/.zshrc
	fi
	if [[ -f ".vimrc" ]]; then
		echo .vimrc exist
	else
		curl -LO gitee.com/bsos-top/open/raw/master/.vimrc
	fi
fi

