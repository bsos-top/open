#!/usr/bin/env bash

pip3 config set global.index-url http://pypi.tuna.tsinghua.edu.cn/simple
pip3 config set global.trusted-host pypi.tuna.tsinghua.edu.cn

# pip3 config set global.index-url http://mirrors.aliyun.com/pypi/simple
# pip3 config set global.trusted-host mirrors.aliyun.com
