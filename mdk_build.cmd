@echo off
if exist mdk.uvproj (
	set BUILD_PRJ=mdk.uvproj
) else (
	if exist mdk.uvprojx (
		set BUILD_PRJ=mdk.uvprojx
	) else (
		echo No Project File
		goto END
	)
)
echo Build Project:%BUILD_PRJ%
if "%KEIL_PATH%" == "" (
	if not exist C:\Keil_v5\UV4\UV4.exe (
		echo Not set KEIL_PATH
		goto END
	) else (
		set KEIL_PATH=C:\Keil_v5
	)
)

%KEIL_PATH%\UV4\UV4.exe -j0 -b %BUILD_PRJ% -o build_log.txt
type build_log.txt
echo Build END

:END
if "%1" neq "wait" (
	goto:eof
)
set WAIT_SEC=%2
if "%WAIT_SEC%" equ "" (
	set WAIT_SEC=9
)
if %WAIT_SEC% gtr 9 (
	set WAIT_SEC=9
)
call:getbs bs
set/p="Wait to Exit: "<nul
for /l %%i in (%WAIT_SEC%,-1,0) do (
	set /p="%%i"<nul
	choice /t 1 /d y /n >nul
	set /p="%bs%"<nul
)
goto:eof

:getbs
	for /F %%a in ('"prompt $h&for %%b in (1) do rem"')do Set "%~1=%%a"
goto:eof
