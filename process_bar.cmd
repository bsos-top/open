@echo off
if "%OS%"=="Windows_NT" (
	setlocal EnableDelayedExpansion
) else (
	echo OS is not Windows_NT
	pause
	goto:eof
)


set ItemAll=7
echo ItemAll=!ItemAll!

goto:pro_bar2

goto:eof
:pro_bar2
for /l %%i in (0,1,!ItemAll!) do (
	call:get_cols cols
	set /a cols-=8
	set /a process=!cols!*%%i/!ItemAll!
	set /a rest_i=!ItemAll!-%%i
	set /a rest=!cols!*!rest_i!/!ItemAll!
	set /a rest=!cols!-!process!
	set /a percent_i=100*%%i/!ItemAll!
	set /a percent_f=1000*%%i/!ItemAll!%%10
	set "number=!percent_i!.!percent_f!"
	set /a is_shi=!percent_i!/10
	set /a is_bai=!percent_i!/100
	set /p="[s["<nul
	call:show_item !process! #
	call:show_item !rest! .
	if !is_bai! equ 0 (
		if !is_shi! equ 0 (
			set /p="]  "<nul
		) else (
			set /p="] "<nul
		)
	) else (
		set /p="]"<nul
	)
	set /p="!number!%%"<nul
::	set /a delay_s=!random!/10
	call:sleep 1
	if %%i neq !ItemALL! (
		set /p="[u[K"<nul
	)
)


goto:eof
:pro_bar1
call:getbs bs
for /l %%i in (1,1,!ItemAll!) do (
	call:get_cols cols
	set /a cols-=8
	set /a process=!cols!*%%i/!ItemAll!
	set /a rest_i=!ItemAll!-%%i
	set /a rest=!cols!*!rest_i!/!ItemAll!
	set /a backspace=!cols!+8
	set /a percent_i=100*%%i/!ItemAll!
	set /a percent_f=1000*%%i/!ItemAll!%%10
	set "number=!percent_i!.!percent_f!"
	set /a is_shi=!percent_i!/10
	set /a is_bai=!percent_i!/100
	set /p="["<nul
	call:show_item !process! #
	call:show_item !rest! .
	if !is_bai! equ 0 (
		if !is_shi! equ 0 (
			set /p="]  "<nul
		) else (
			set /p="] "<nul
		)
	) else (
		set /p="]"<nul
	)
	set /p="!number!%%"<nul
::	set /a delay_s=!random!/10
	call:sleep 1
	call:show_item_bs !backspace!
)

:for_done
echo.
pause
goto:eof

:show_item_bs
set strTemp=
for /l %%i in (1,1,%1) do (
	set "strTemp=!strTemp!%bs%"
)

set /p="!strTemp!"<nul
goto:eof

:show_item
set strTemp=
for /l %%i in (1,1,%1) do (
	set "strTemp=!strTemp!%2"
)

set /p="!strTemp!"<nul
goto:eof

:getbs 
for /F %%a in ('"prompt $h&for %%b in (1) do rem"')do Set "%~1=%%a"
goto:eof

:sleep
timeout /t %1 > nul
goto:eof

:get_cols
for /f "tokens=*" %%i in ('mode ^| find "Columns:"') do (
	set gotStr="%%i"
	set gotStr=!gotStr:Columns:=!
	set gotStr=!gotStr: =!
	set gotStr=!gotStr:"=!
)

Set "%~1=!gotStr!"
::Set "%~1=10"
goto:eof

