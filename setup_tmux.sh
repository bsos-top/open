#!/usr/bin/env bash
# common tmux.tar.gz
tar xzf tmux.tar.gz
sudo yum -y install ncurses-devel
sudo yum -y install libevent-devel
tar xzf tmux-3.3.tar.gz
cd tmux-3.3
./configure && make
sudo make install
cd ..
rm -rf tmux-3.3
