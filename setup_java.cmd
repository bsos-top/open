@echo off

mkdir C:\Programs\java
cd/d C:\Programs\java
if not exist openjdk1.8.0-292 (
  wget --no-check-certificate https://www.winstonkenny.cn/d/F6r9GA0T -O openjdk1.8.0-292.exe
  openjdk1.8.0-292.exe
  del openjdk1.8.0-292.exe
) else (
  echo openjdk1.8.0-292 exist
  pause
)
mkdir C:\Programs\maven
cd/d C:\Programs\maven
if not exist apache-maven-3.8.5 (
  wget --no-check-certificate https://www.winstonkenny.cn/d/D1Ko0Ayc -O apache-maven-3.8.5-bin.exe
  apache-maven-3.8.5-bin.exe
  del apache-maven-3.8.5-bin.exe
) else (
  echo apache-maven-3.8.5 exist
  pause
)

exit/b

