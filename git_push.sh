#!/bin/bash
REMOTE_INPUT=$1

#THIS_BRANCH=`git branch --show-current 2>/dev/null`
#THIS_BRANCH=`git branch 2>/dev/null | awk '/^\*/{print $2}'` # support git old version
THIS_BRANCH=`git rev-parse --abbrev-ref HEAD`
echo Now Branch:$THIS_BRANCH

count=0
for FOR_EACH in $(git remote | grep -v '^ *#')
do
  if [ $count = 0 ];then
    REMOTE=$FOR_EACH
  fi
  count=$(($count+1))
  echo "* remote: [32m$FOR_EACH[m"
  echo $(git remote show $FOR_EACH | grep "Fetch URL:")
done

if [ $count = 0 ];then
  exit 1
fi

git add --all .
git commit -m "update"

if [ $count -gt 1 ];then
  echo ">>>"
  git remote
  echo "<<<"
  if [ "$REMOTE_INPUT" == "all" ];then
    REMOTE_INPUT=""
  else
    read -p "Input a remote(all):" REMOTE_INPUT
  fi
  if [ -n "$REMOTE_INPUT" ];then
    REMOTE=$REMOTE_INPUT
  else
    echo Goto push ALL...
    for FOR_EACH in $(git remote | grep -v '^ *#')
    do
      echo
      echo Now Push [32m$FOR_EACH[0m
      echo
      git push $FOR_EACH $THIS_BRANCH
    done
    exit $?
  fi
fi

echo
echo Now Push [32m$REMOTE[0m
echo
git push $REMOTE $THIS_BRANCH

