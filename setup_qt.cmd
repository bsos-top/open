@echo off

mkdir C:\Programs\Qt\Qt5.12.12\5.12.12

cd C:\Programs\Qt\Qt5.12.12\5.12.12

if not exist msvc2017_64 (
  call curl --ssl-no-revoke -L https://www.winstonkenny.cn/d/1ylG0A9X -o qt5.12.12_msvc2017_64.exe
  qt5.12.12_msvc2017_64.exe
  if %errorlevel% equ 0 (
    del qt5.12.12_msvc2017_64.exe
  )
)

if not exist msvc2017 (
  call curl --ssl-no-revoke -L https://www.winstonkenny.cn/d/mx4iSMlH -o qt5.12.12_msvc2017.exe
  qt5.12.12_msvc2017.exe
  if %errorlevel% equ 0 (
    del qt5.12.12_msvc2017.exe
  )
)

exit/b

