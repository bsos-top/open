#!/bin/bash
REMOTE_INPUT=$1

#THIS_BRANCH=`git branch 2>/dev/null | awk '/^\*/{print $2}'` # support git old version
THIS_BRANCH=`git rev-parse --abbrev-ref HEAD`
echo Now Branch:$THIS_BRANCH

count=0
for FOR_EACH in $(git remote | grep -v '^ *#')
do
  if [ $count = 0 ];then
    REMOTE=$FOR_EACH
  fi
  count=$(($count+1))
  echo "* remote: [32m$FOR_EACH[m"
  #echo $(git remote show $FOR_EACH | grep "Fetch URL:")
done

if [ $count = 0 ];then
  exit 1
fi

if [ $count -gt 1 ];then
  echo ">>>"
  git remote
  echo "<<<"
  if [[ "$REMOTE_INPUT" == "first" ]];then
    REMOTE_INPUT=""
  else
    if [ -z "$REMOTE_INPUT" ];then
      read -p "Input a remote($REMOTE):" REMOTE_INPUT
    fi
  fi

  if [ -n "$REMOTE_INPUT" ];then
    REMOTE=$REMOTE_INPUT
  fi
fi

git pull $REMOTE $THIS_BRANCH

