# .bashrc

export CLICOLOR=1
export LSCOLORS="gxfxaxdxcxegedabagacad"
export HISTCONTROL=ignoreboth
export HISTIGNORE="ls -lha"
export LANG=en_US.UTF-8

alias ll="ls -lh"
alias la="ls -lha"
alias where="whereis"

OS_ID=$(grep -E '^ID=' /etc/os-release 2>/dev/null | awk -F'=' '{gsub(/"/, "", $2); print $2}')

shopt -s checkwinsize
case "$TERM" in
  xterm-color|*-256color) color_prompt=yes;;
esac
if [ -x /usr/bin/dircolors ]; then
  test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
  alias ls='ls --color=auto'
  alias grep='grep --color=auto'
  alias fgrep='fgrep --color=auto'
  alias egrep='egrep --color=auto'
fi

# Source global definitions
if [ -f /etc/profile ]; then
  . /etc/profile
fi
if [ -f /etc/bashrc ]; then
  . /etc/bashrc
fi

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions
git-branch-name() {
  git symbolic-ref HEAD 2>/dev/null | cut -d"/" -f 3
}
get-ps() {
  local errorlevel=$?
  local show_errorlevel=""
  local branch="$(git rev-parse --abbrev-ref HEAD 2>/dev/null)"
  local tag=$(git tag --points-at HEAD 2>/dev/null)
  if [ -n "${tag}" ];then
    branch=${tag}
  fi
  local status=`git status -s 2>/dev/null`
  local show_status=""
  if [ $branch ]; then
    if [[ $status == "" ]];then
      show_status="32mο"
    else
      show_status="31m✗"
    fi
    if [ "$errorlevel" != "0" ];then
      printf "on git:\e[36m%s\e[m \e[%s\e[m [$(date "+%T")] C:\e[31m%s\e[m" "$branch" $show_status $errorlevel
    else
      printf "on git:\e[36m%s\e[m \e[%s\e[m [$(date "+%T")]" "$branch" $show_status
    fi
  else
    if [ "$errorlevel" != "0" ];then
      printf "[$(date "+%T")] C:\e[31m%s\e[m" $errorlevel
    else
      printf "[$(date "+%T")]"
    fi
  fi
}

# can not use function to select color, because errorlevel
if [[ $(whoami) == "root" ]];then
    PS1="\n\[\033[1;34m\]#\[\033[m\] \[\033[30;43m\]\u\[\033[m\] \[\033[0;37m\]@\[\033[m\] \[\033[0;32m\]${HOSTNAME%.*}\[\033[m\].\[\033[0;35m\]${HOSTNAME%.*}\[\033[m\] \[\033[0;37m\]in\[\033[m\] \[\033[0;93m\]\w\[\033[m\] \$(get-ps)\n\[\033[1;31m\]\$\[\033[0m\] "
else
    PS1="\n\[\033[1;34m\]#\[\033[m\] \[\033[00;36m\]\u\[\033[m\] \[\033[0;37m\]@\[\033[m\] \[\033[0;32m\]${HOSTNAME%.*}\[\033[m\].\[\033[0;35m\]${HOSTNAME##*.}\[\033[m\] \[\033[0;37m\]in\[\033[m\] \[\033[0;93m\]\w\[\033[m\] \$(get-ps)\n\[\033[1;31m\]\$\[\033[0m\] "
fi

hash bind 2>/dev/null
if [ "$?" == "0" ];then
  # check line editing
  if [[ $(set -o | grep ^emacs | grep on) != "" ]];then
    bind '"\e\e":"\C-asudo \C-e"'
    bind '"\e-":"\C-a\e[C\e[C\e[C\e[C\e[C\C-u\C-e"'
  fi
fi

if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi


PATH=$PATH:~/.shell

[ -f ~/.env ] && source ~/.env
[ -f ~/.alias ] && source ~/.alias
export TERM=xterm-256color
