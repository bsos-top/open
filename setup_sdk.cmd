@echo off
:: ==================================
:: - batchfile for windows
:: ==================================
set "error=1"
set "old_dir=%cd%"
set "script_path=%~dp0"
if "%script_path:~-1%"=="\" set "script_path=%script_path:~0,-1%"
cd /d "%script_path%"
if exist ".env.cmd" (
  call .env.cmd
)
:: ==================================
:: core process start

if not exist C:\Programs\sdk (
  mkdir C:\Programs\sdk
)
cd C:\Programs\sdk
echo Now in Dir:%cd%
if not exist platforms (
  echo Now download android-sdk-platforms-android-30.exe
  call curl --ssl-no-revoke -L https://www.winstonkenny.cn/d/NnXm07Zc -o android-sdk-platforms-android-30.exe
  android-sdk-platforms-android-30.exe -oplatforms
  echo Done
) else (
  echo platforms is in sdk dir, do not download
)
if not exist build-tools (
  echo Now download android-sdk-build-tools-30.0.3.exe
  call curl --ssl-no-revoke -L https://www.winstonkenny.cn/d/zjDmGK5S -o android-sdk-build-tools-30.0.3.exe
  android-sdk-build-tools-30.0.3.exe -obuild-tools
  echo Done
) else (
  echo build-tools is in sdk dir, do not download
)
if not exist platform-tools (
  echo Now download platform-tools_r33.0.3-win.exe
  call curl --ssl-no-revoke -L https://www.winstonkenny.cn/d/VluO1wAt -o platform-tools_r33.0.3-win.exe
  platform-tools_r33.0.3-win.exe
  echo Done
)
if not exist tools (
  echo Now download android-sdk-tools-lib.exe
  call curl --ssl-no-revoke -L https://www.winstonkenny.cn/d/szpTvIa9 -o android-sdk-tools-lib.exe
  android-sdk-tools-lib.exe -otools
  echo Done
) else (
  echo tools is in sdk dir, do not download
)

:: core process end
:: ==================================
:eof
set "error=0"
:return
cd "%old_dir%"
pause
exit /b %error%
