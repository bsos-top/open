@echo off
if "%OS%"=="Windows_NT" (setlocal EnableDelayedExpansion) else goto:eof
set arg_f=
set arg_item=
set port=
set destination=
set PortParam=
set file=

:loop
if "%1" neq "" (
  set arg=%1
  if "!arg:~0,1!" == "-" (
    if "!arg!" == "-u" (
      shift
      set arg_f=u
      set arg_item=u
      goto:loop
    ) else (
      if "!arg!" == "-d" (
        set arg_f=d
        set arg_item=d
        shift
        goto:loop
      ) else (
        if "!arg!" == "-p" (
          set arg_f=p
          shift
          goto:loop
        ) else (
          shift
          goto:loop
        )
      )
    )
  ) else (
    if "!arg_f!" == "u" (
      set file=!arg!
      set arg_f=
      shift
      goto:loop
    ) else (
      if "!arg_f!" == "d" (
        set file=!arg!
        set arg_f=
        shift
        goto:loop
      ) else (
        if "!arg_f!" == "p" (
          set port=!arg!
          set arg_f=
          shift
          goto:loop
        ) else (
          set destination=!arg!
          set arg_f=
          shift
          goto:loop
        )
      )
    )
  )
)

for /f "tokens=1,2 delims=@" %%i in ("!destination!") do (
  if "%%j" == "" (
    set host=%%i
  ) else (
    set user=%%i
    set host=%%j
  )
)

if not "!port!" == "" (
  if "!arg_item!" == "" (
    set "PortParam=-p !port! "
  ) else (
    set "PortParam=-P !port! "
  )
)

if "!arg_item!" == "d" ( scp %PortParam%!destination!:!file! .& goto:end_if )
if "!arg_item!" == "u" ( scp %PortParam%!file! !destination!:&  goto:end_if )
if "!arg_item!" == ""  ( ssh %PortParam%!destination!&          goto:end_if )
:end_if

