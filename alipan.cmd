@echo off

if "%PY_HOME%" == "" (
  set PY_HOME=C:\Programs\Python\Python37
)
if not exist %PY_HOME%\python.exe (
  curl -LO gitee.com/bsos-top/open/raw/master/setup_python37.cmd
  call setup_python37.cmd
  del setup_python37.cmd
)
%PY_HOME%\python.exe %userprofile%\.shell\alipan.py %*

