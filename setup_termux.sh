#!/usr/bin/env bash
echo ' _____
|_   _|__ _ __ _ __ ___  _   ___  __
  | |/ _ \ '\'__\| \''_ ` _ \| | | \ \/ /
  | |  __/ |  | | | | | | |_| |>  <
  |_|\___|_|  |_| |_| |_|\__,_/_/\_\
' > $PREFIX/etc/motd

sed -i 's@^\(deb.*stable main\)$@#\1\ndeb https://mirrors.tuna.tsinghua.edu.cn/termux/termux-packages-24 stable main@' $PREFIX/etc/apt/sources.list
sed -i 's@^\(deb.*games stable\)$@#\1\ndeb https://mirrors.tuna.tsinghua.edu.cn/termux/game-packages-24 games stable@' $PREFIX/etc/apt/sources.list.d/game.list
sed -i 's@^\(deb.*science stable\)$@#\1\ndeb https://mirrors.tuna.tsinghua.edu.cn/termux/science-packages-24 science stable@' $PREFIX/etc/apt/sources.list.d/science.list
pkg update
pkg install -y vim curl wget git tree openssh iproute2 util-linux zsh

cd
curl -LO gitee.com/bsos-top/open/raw/master/termux-ohmyzsh.tar.gz
curl -LO gitee.com/bsos-top/open/raw/master/.zshrc
curl -LO gitee.com/bsos-top/open/raw/master/.vimrc
tar xzf termux-ohmyzsh.tar.gz
rm termux-ohmyzsh.tar.gz
chsh -s zsh

