@echo off
call senv
7z x gradle-6.8.2-bin.exe
mkdir D:\BSOS\Android\Sdk
mkdir C:\Programs\gradle
copy gradle-6.8.2-bin.zip C:\Programs\gradle
del gradle-6.8.2-bin.zip

sed -i "s/hw.dPad = no/hw.dPad = yes/g" %USERPROFILE%\.android\avd\*.avd\config.ini
sed -i "s/hw.mainKeys = no/hw.mainKeys = yes/g" %USERPROFILE%\.android\avd\*.avd\config.ini

