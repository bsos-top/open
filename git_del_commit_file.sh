#!/usr/bin/sh
if [ $# == 0 ];then
  git rev-list --all | xargs -rL1 git ls-tree -r --long |
    sort -uk3 | sort -rnk4 | head -10 |
    #awk '{printf "%-30s %9s %s\n",$3,"`byte2size "$4"`",$5}'
    #awk '{cmd = "echo -e \""$3" `byte2size "$4"` "$5"\"";system(cmd);}'
    awk '{cmd = "echo -e \""$3" $(printf %9s `byte2size "$4"`) "$5"\"";system(cmd);}'
  exit $?
fi

git filter-branch -f --index-filter 'git rm --cached --ignore-unmatch $1'
rm -rf .git/refs/original/
git reflog expire --expire=now --all
git fsck --full --unreachable
git repack -A -d
git gc --aggressive --prune=now
echo "please run \"git push --force [remote] master\""

