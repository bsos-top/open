#!/bin/bash
ETHNAME=$1
read -p "Please Input username:" USERNAME
stty -echo
read -p "Please Input password:" PASSWORD
stty echo

echo ctrl_interface=/var/run/wpa_supplicant > ~conf.conf
echo ap_scan=0 >> ~conf.conf
echo network={ >> ~conf.conf
echo \ \ \ \ key_mgmt=IEEE8021X >> ~conf.conf
echo \ \ \ \ eap=PEAP >> ~conf.conf
echo \ \ \ \ identity=\"$USERNAME\" >> ~conf.conf
echo \ \ \ \ password=\"$PASSWORD\" >> ~conf.conf
echo \ \ \ \ phase2=\"autheap=MSCHAPV2\" >> ~conf.conf
echo } >> ~conf.conf

wpa_supplicant -B -i $ETHNAME -c ~conf.conf -D wired
rm -f ~conf.conf
sleep 1
echo Now Dhclient...
dhclient $ETHNAME
echo Dhclient OK!

