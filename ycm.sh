tar xzf Python3.7.9
cd Python3.7.9
./configure --prefix=/usr/local/python/python37 --enable-shared --enable-optimizations
make -j3
sudo make install
sudo cp libpython3.7m.so.1.0 /usr/local/lib64
sudo cp libpython3.7m.so.1.0 /usr/lib
sudo cp libpython3.7m.so.1.0 /usr/lib64
sudo ln -s /usr/local/python/python37/bin/python3 /usr/bin/python3
python3 -V

# 1. Install a package with repository for your system:
# On CentOS, install package centos-release-scl available in CentOS repository:
sudo yum install centos-release-scl
# On RHEL, enable RHSCL repository for you system:
sudo yum-config-manager --enable rhel-server-rhscl-7-rpms
# 2. Install the collection:
sudo yum install devtoolset-8
# 3. Start using software collections:
scl enable devtoolset-8 bash

python3 install.py --all

