@echo off
if not exist %userprofile%\.shell (
  mkdir %userprofile%\.shell
)

net session 2>nul>nul
if %errorlevel% neq 0 (
  echo [31mRequires administrator to run[0m
  pause
  goto:eof
)
setlocal
set with_space=0
set param=%1
echo %param: =*% | findstr "*" > nul && (
  set with_space=1
)
if %with_space% == 1 (
  echo [31mDo not support with space[0m
  goto:eof
)

call:_remove_nodouble_quotation_exist_space %1 __FILETAG__
if "%__FILETAG__%" == "" (
  set/p __FILETAG__=Please input a file tag: 
  if "%__FILETAG__%" == "" (
    echo [31mYou must input a file tag[0m
    goto:eof
  )
)

if "%__FILETAG__%" == "--all" (
  call:link_setup
  call:link_git
  call:link_vim
  call:link_alipan
  call:link_wt
  goto:eof
)

if "%__FILETAG__%" == "wt" (
  call:link_wt
  goto:eof
)
if "%__FILETAG__%" == "setup" (
  call:link_setup
  goto:eof
)
if "%__FILETAG__%" == "git" (
  call:link_git
  goto:eof
)
if "%__FILETAG__%" == "vim" (
  call:link_vim
  goto:eof
)
if "%__FILETAG__%" == "alipan" (
  call:link_alipan
  goto:eof
)

call:_link "%__FILETAG__%"
goto:eof
:: ================
:: === function ===
:: ================
:link_setup
  call:_link common.cmd
  call:_link setup.cmd
  call:_link setup_env.cmd
  call:_link senv.cmd
  call:_link seqt.cmd
  call:_link sepy.cmd
  call:_link segcc.cmd
  call:_link sego.cmd
  call:_link shcp.cmd
  call:_link ll.cmd
  call:_link la.cmd
  call:_link cdc.cmd
  call:_link cdd.cmd
  call:_link cde.cmd
  call:_link cdf.cmd
  call:_link cdpb.cmd
  call:_link cdpd.cmd
  call:_link cdps.cmd
  call:_link cdsu.cmd
  call:_link cdsys.cmd
  call:_link ch.cmd
  call:_link ip.cmd
  call:_link r_.cmd
goto:eof
:link_wt
  set _FILE_PATH_=%windir%\System32\
  call:_link wt.cmd
  set _FILE_PATH_=%userprofile%\AppData\Local\Microsoft\Windows Terminal\
  call:_link settings.json
  set _FILE_PATH_=%userprofile%\AppData\Local\Packages\Microsoft.WindowsTerminal_8wekyb3d8bbwe\LocalState\
  call:_link settings.json
goto:eof
:link_git
  call:_link git_push.cmd gpsh.cmd
  call:_link git_pull.cmd gpll.cmd
  call:_link git_push.cmd
  call:_link git_pull.cmd
  call:_link git_status.cmd gst.cmd
  call:_link git_status.cmd
  call:_link git_file.cmd
  call:_link git_dir_add.cmd
  call:_link git_del_commit_history.cmd
goto:eof
:link_vim
  call:_link vim.cmd
  set _FILE_PATH_=%userprofile%\
  call:_link .vimrc
  call:_link .ideavimrc
goto:eof
:link_alipan
  call:_link alipan.cmd
  set _FILE_PATH_=%userprofile%\.shell\
  call:_link alipan.py
goto:eof
:_link
  ::set file_last=%_FILE_PATH_:~-1,1%
  ::if not %file_last% == "\" (
  :: set _FILE_PATH_=%_FILE_PATH_%\
  ::)
  call:_remove_nodouble_quotation_exist_space %1 _FILE_
  if "%2" == "" (
    set _FILE_TARGET_=%_FILE_%
  ) else (
    call:_remove_nodouble_quotation_exist_space %2 _FILE_TARGET_
  )

  if not exist "%cd%\%_FILE_%" (
    echo [31mError[m File^([32m%cd%\%_FILE_%[m^) not exist
    goto:eof
  )
  set _FILE_TYPE_=%_FILE_:~-4%
  :: *.cmd *.bat *.exe mklink in  and %userprofile%\.shell
  if "%_FILE_%" == "wt.cmd" (
    call:_link_static "%_FILE_%" "%_FILE_TARGET_%"
    goto:eof
  )
  if "%_FILE_TYPE_%" == ".cmd" (
    call:_link_command "%_FILE_%" "%_FILE_TARGET_%"
    goto:eof
  )
  if "%_FILE_TYPE_%" == ".bat" (
    call:_link_command "%_FILE_%" "%_FILE_TARGET_%"
    goto:eof
  )
  if "%_FILE_TYPE_%" == ".exe" (
    call:_link_command "%_FILE_%" "%_FILE_TARGET_%"
    goto:eof
  )
  call:_link_static "%_FILE_%" "%_FILE_TARGET_%"
goto:eof

:_link_command
  call:_remove_nodouble_quotation_exist_space %1 _FILE_
  if not "%2" == "" (
    call:_remove_nodouble_quotation_exist_space %2 _FILE_TARGET_
  ) else (
    set _FILE_TARGET_=%_FILE_%
  )
  set _FILE_PATH_=%programdata%\Bin-Win64\
  call:_link_static "%_FILE_%" "%_FILE_TARGET_%"
  set _FILE_PATH_=%userprofile%\.shell\
  call:_link_static "%_FILE_%" "%_FILE_TARGET_%"
goto:eof

:_link_static
  call:_remove_nodouble_quotation_exist_space %1 _FILE_
  call:_remove_nodouble_quotation_exist_space %2 _FILE_TARGET_
  if exist "%_FILE_PATH_%%_FILE_TARGET_%" (
    del "%_FILE_PATH_%%_FILE_TARGET_%"
  )
  mklink "%_FILE_PATH_%%_FILE_TARGET_%" "%cd%\%_FILE_%"
  echo Linked File^([34m%_FILE_PATH_%%_FILE_TARGET_%[m -^> [32m%cd%\%_FILE_%[m^)
goto:eof

:_remove_nodouble_quotation_exist_space
  set __INPUT__=%1
  set __INPUT_FIRST_LAST__=%__INPUT__:~0,1%%__INPUT__:~-1,1%
  if "%__INPUT_FIRST_LAST__%" == """" (
    set __INPUT__=%__INPUT__:~1,-1%
  )
  set "%~2=%__INPUT__%"
goto:eof

