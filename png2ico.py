#!/usr/bin/env python
import sys
import getopt
import PythonMagick

def main():
    opts, args = getopt.getopt(sys.argv[1:], "hi:o:s:", ["help", "output=", "input=", "sample="])
    filename=''
    for arg in args:
        filename = arg
    if filename != '':
        img = PythonMagick.Image(filename+'.png')
        img.sample('16x16')
        img.write(filename+'.ico')
    else:
        print('no file')

if __name__ == "__main__":
    main()

