#!/usr/bin/sh
if [ $# -lt "2" ];then
  echo "You must input args like (makefor.sh 123.txt 1M)"
  exit 1
fi
SPLIT_SIZE=1M
#if [[ "$2" =~ ^[1-9]+[0-9]*M$ ]];then
#  echo "Split File[[32m$1[m]Size[[35m$2[m]"
#else
#  echo "You must input a size like 1M or 100M after File"
#  exit 1
#fi
FileName=$1
if [ $3 == "del" ];then
  echo "[31mDeleting"
  ls -l $FileName* | awk '{print "[35mFile[m:[32m" $NF}' | grep -v -E "$FileName$"
  echo -e "[m\c"
  rm `ls -l $FileName* | awk '{print $NF}' | grep -v -E "$FileName$"`
  echo [31mEnd[m
  exit 0
fi
split -b ${SPLIT_SIZE} -d $FileName $FileName
ls -l $FileName* | awk '{print $NF}'
md5sum $FileName | awk '{print $1}' > $FileName.md5
echo MD5:$(cat $FileName.md5)

