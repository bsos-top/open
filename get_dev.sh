#!/bin/bash
NET_INTERFACE=$1
if [[ "$1" == "" ]];then
        echo "please input a net_interface"
        exit 1
fi

ID_PATH=$(udevadm info /sys/class/net/$NET_INTERFACE | grep ID_PATH=)
if [[ "$?" != "0" ]];then
        exit 1
fi

ID_PATH=${ID_PATH#*=}
echo -e "\033[31mNET_INTERFACE\033[0m:\033[32m$NET_INTERFACE\033[0m"
echo -e "\033[34mID_PATH\033[0m:=$ID_PATH"
ID_PATH=${ID_PATH%:*}:
ls -l /dev/serial/by-path/ | grep $ID_PATH

