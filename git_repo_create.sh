#!/bin/bash

TKEN=$1
NAME=$2
PATH=$3
NSID=$4
DESP=$5

DataJson='{
	"name"="'$NAME'",
	"path"="'$PATH'",
	"namespace_id"="'$NSID'",
	"description"="'$DESP'"
}'
echo $DataJson
exit 0

curl -X POST \
  -H 'Content-Type:application/json' \
  -d $DataJson \
  https://git.winstonkenny.cn/api/v4/projects?access_token=$TKEN

