#!/bin/sh
totalsize=$1
if [[ "$totalsize" =~ ^[0-9]+$ ]];then
  if [ 1024 -gt $totalsize ];then
    size="$totalsize"B
  elif [ 1048576 -gt $totalsize ];then
    size=`echo  "scale=3; a = $totalsize / 1024 ; if (length(a)==scale(a)) print 0;print a"  | bc `
    size="$size"KB
  elif [ 1073741824 -gt $totalsize ];then
    size=`echo  "scale=3; a = $totalsize / 1048576 ; if (length(a)==scale(a)) print 0;print a"  | bc `
    size="$size"MB
  elif [ 1073741824 -le $totalsize ];then
    size=`echo  "scale=3; a = $totalsize / 1073741824 ; if (length(a)==scale(a)) print 0;print a"  | bc `
    size="$size"GB
  else
    size="0"
  fi
else
  exit 1
fi
echo $size

