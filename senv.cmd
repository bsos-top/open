@echo off
chcp 65001 > nul
::chcp 437 > nul :: en
::chcp 936 > nul :: zh
if "%OS%" neq "Windows_NT" (
  echo OS is not Windows_NT
  pause
  goto:eof
)

:: BatchFile not support path=%path%; in ()
::if not "%path_last%" == ";" (
::  path=%path%;
::)
if "%path:~-1%" == ";" (
  goto:end_path_add_
)
path=%path%;
:end_path_add_

echo %path% | findstr/c:"%userprofile%\\.shell;" >nul && (
  goto:end_path_add_shell
)
path=%path%%userprofile%\.shell;
:end_path_add_shell

echo %path% | findstr/c:"%programdata%\\Bin-Win64;" >nul && (
  goto:end_path_add_bin_win64
)
path=%path%%programdata%\Bin-Win64;
:end_path_add_bin_win64

echo %path% | findstr/c:"%cd%;" >nul && (
  goto:end_path_add_this_dir
)
path=%path%%cd%;
:end_path_add_this_dir

net session 2>nul>nul
if %errorlevel% equ 0 (
  PROMPT=[34m#[0m [30;43m%USERNAME%[0m [37@[0m [32m%COMPUTERNAME%[0m[31m^(CMD^)[0m [37min[0m [33m$p[0m
  rem [$t]$_[0m[31m$$[0m$s
) else (
  PROMPT=[34m#[0m [36m%USERNAME%[0m [37m@[0m [32m%COMPUTERNAME%[0m[31m^(CMD^)[0m [37min[0m [33m$p[0m
  rem [$t]$_[0m[31m$$[0m$s
)

rem cd USERPROFILE dir when dir is C:\Windows\System32
if "%cd%" == "C:\Windows\System32" (
  cd /d %USERPROFILE%
) else if "%cd%" == "C:\Windows\system32" (
  cd /d %USERPROFILE%
)

%programdata%\Bin-Win64\clink\clink.bat inject --autorun
